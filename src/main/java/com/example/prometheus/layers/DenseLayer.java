package com.example.prometheus.layers;

import com.example.prometheus.LayerType;
import com.example.prometheus.Neuron;
import com.example.prometheus.Utils;
import com.example.prometheus.activations.Activate;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

public class DenseLayer<T extends Activate> {

    private List<Neuron<T>> neurons;

    private List<Double> bottomLayerDeSlashDy;

    private int numberOfInputs;
    private int numberOfOutputs;
    private T activation;
    private LayerType layerType;

    public DenseLayer(int numberOfInputs, int numberOfOutputs, T activation, LayerType layerType) {
        this.numberOfInputs = numberOfInputs;
        this.numberOfOutputs = numberOfOutputs;
        this.activation = activation;
        this.layerType = layerType;

        neurons = initNeurons(numberOfOutputs);
        bottomLayerDeSlashDy = Utils.initArrayListToZero(numberOfInputs);
    }

    public List<Double> feedForward(List<Double> inputs) {
       List<Double> outList = new ArrayList<>();
       neurons.stream().forEach(neuron -> {
           Double outDouble = neuron.feedForward(inputs);
           outList.add(outDouble);
       });
       return outList;
    }

    public List<Double> backProp(List<Double> dESlashdYj) {
        bottomLayerDeSlashDy = Utils.initArrayListToZero(numberOfInputs);
        for (int i = 0; i < neurons.size(); i++) {
            List<Double> dESlashDYi = neurons.get(i).backProp(dESlashdYj.get(i));
            for (int j = 0; j < numberOfInputs; j++) {
                bottomLayerDeSlashDy.set(j, bottomLayerDeSlashDy.get(j) + dESlashDYi.get(j));
            }
        }
        return bottomLayerDeSlashDy;
    }

    private List<Neuron<T>> initNeurons(int numberOfOutputs) {
        List<Neuron<T>> returnList = new ArrayList<>();
        for (int i = 0; i < numberOfOutputs; i++) {
            returnList.add(new Neuron<T>(i, activation, numberOfInputs));
        }
        return returnList;
    }

    public void recToString() {
        LoggerFactory.getLogger(DenseLayer.class).info("Layer " + layerType);
        for (int i = 0; i < neurons.size(); i++) {
            neurons.get(i).recToString();
        }
    }
}
