package com.example.prometheus.examples;

import com.example.prometheus.LayerType;
import com.example.prometheus.Model;
import com.example.prometheus.ModelBuilder;
import com.example.prometheus.activations.Sigmoid;
import com.example.prometheus.layers.DenseLayer;
import org.slf4j.LoggerFactory;

import java.util.*;

public class BarPlay {

    public void play() {

        ModelBuilder modelBuilder = new ModelBuilder();

        //net arch
        final Model model = modelBuilder.addLayer(new DenseLayer(5, 100, Sigmoid.getINSTANCE(), LayerType.INPUT))
                .addLayer(new DenseLayer(100, 100, Sigmoid.getINSTANCE(), LayerType.MIDDLE))
                .addLayer(new DenseLayer(100, 5, Sigmoid.getINSTANCE(), LayerType.OUTPUT)).build();

        //prepare data
        List<List<Double>> inputsAll = new ArrayList<>();
        List<List<Double>> outputsAll = new ArrayList<>();
        Random random = new Random();

        double[] inputsArr = {0.0,0.0,0.0,0.0,0.0};
        for (int i = 0; i < 100; i++) {
            inputsArr = new double[]{0.0,0.0,0.0,0.0,0.0};
            int next1 = random.nextInt(4);//0-3
            int next2 = random.nextInt(10);//0-9
            inputsArr[next1] = (10 - next2) / 10.0;
            inputsArr[next1 + 1] = (next2) / 10.0;

            List<Double> inputList = new ArrayList<>();
            inputList.add(inputsArr[0]);
            inputList.add(inputsArr[1]);
            inputList.add(inputsArr[2]);
            inputList.add(inputsArr[3]);
            inputList.add(inputsArr[4]);
            inputsAll.add(inputList);

            List<Double> outputList = new ArrayList<>();
            long nextX = Math.round(next1 + next2/10.0);
            outputList.add(nextX == 0 ? 1.0 : 0.0);
            outputList.add(nextX == 1 ? 1.0 : 0.0);
            outputList.add(nextX == 2 ? 1.0 : 0.0);
            outputList.add(nextX == 3 ? 1.0 : 0.0);
            outputList.add(nextX == 4 ? 1.0 : 0.0);

            outputsAll.add(outputList);
        }

        //train
        model.trainAll(inputsAll, outputsAll);


        //predict

        List<Double> plist = new ArrayList<>();
        plist.add(0.9);
        plist.add(0.1);
        plist.add(0.0);
        plist.add(0.0);
        plist.add(0.0);

        List<Double> predict = model.predict(plist);

    }


    public void play2() {

        ModelBuilder modelBuilder = new ModelBuilder();

        //net arch
        final Model model = modelBuilder.addLayer(new DenseLayer(3, 7, Sigmoid.getINSTANCE(), LayerType.INPUT))
                //.addLayer(new DenseLayer(3, 3, Sigmoid.getINSTANCE(), LayerType.MIDDLE))
                .addLayer(new DenseLayer(7, 3, Sigmoid.getINSTANCE(), LayerType.OUTPUT)).build();

        //prepare data
        List<List<Double>> inputsAll = new ArrayList<>();
        List<List<Double>> outputsAll = new ArrayList<>();
        Random random = new Random();

        double[] inputsArr = {0.0,0.0};
        for (int i = 0; i < 700; i++) {
            inputsArr = new double[]{-10.0, -10.0, -10.0};
            int next1 = random.nextInt(3);//0-3
            int next2 = random.nextInt(10);//0-9
            inputsArr[next1] = 10.0; //(10 - next2) / 10.0;
            //inputsArr[next1 + 1] = (next2) / 10.0;

            List<Double> inputList = new ArrayList<>();
            inputList.add(inputsArr[0]);
            inputList.add(inputsArr[1]);
            inputList.add(inputsArr[2]);
            inputsAll.add(inputList);

            List<Double> outputList = new ArrayList<>();
            //long nextX = Math.round(next1 + next2/10.0);
            outputList.add(next1 == 0 ? 1.0 : 0.0);
            outputList.add(next1 == 1 ? 1.0 : 0.0);
            outputList.add(next1 == 2 ? 1.0 : 0.0);

            outputsAll.add(outputList);
        }

        //train
        model.trainAll(inputsAll, outputsAll);


        //predict

        List<Double> plist = new ArrayList<>();
        plist.add(10.0);
        plist.add(-10.0);
        plist.add(-10.0);

        List<Double> predict = model.predict(plist);
        LoggerFactory.getLogger(BarPlay.class).info("predict 100: " + predict.get(0) + ", " + predict.get(1)+ ", " + predict.get(2));


        plist = new ArrayList<>();
        plist.add(-10.0);
        plist.add(-10.0);
        plist.add(10.0);

        List<Double> predict1 = model.predict(plist);
        LoggerFactory.getLogger(BarPlay.class).info("predict 001: " + predict1.get(0) + ", " + predict1.get(1)+ ", " + predict1.get(2));

        plist = new ArrayList<>();
        plist.add(-10.0);
        plist.add(10.0);
        plist.add(-10.0);

        List<Double> predict2 = model.predict(plist);
        LoggerFactory.getLogger(BarPlay.class).info("predict 010: " + predict2.get(0) + ", " + predict2.get(1)+ ", " + predict2.get(2));
    }


}
