package com.example.prometheus;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {

    public static List<Double> initArrayListToZero(int number) {
        List<Double> returnList = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            returnList.add(Double.valueOf(0.0));
        }
        return returnList;
    }

    public static List<Double> initArrayListToRandom(int number) {
        List<Double> returnList = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < number; i++) {
            returnList.add(random.nextInt(11)/10.0);
        }
        return returnList;
    }

}
