package com.example.prometheus;

import com.example.prometheus.activations.Activate;
import com.example.prometheus.layers.DenseLayer;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.example.prometheus.Utils.initArrayListToRandom;
import static com.example.prometheus.Utils.initArrayListToZero;

public class Neuron<T extends Activate> {

    private List<Double> inputs;
    private List<Double> weights;
    private int indexInLayer;
    private T activate;
    private double output;
    private int numberOfInputsAndWeights;
    private List<Double> dESlashDyi;




    public Neuron(int indexInLayer, T activate, int numberOfInputsAndWeights) {
        this.indexInLayer = indexInLayer;
        this.activate = activate;
        this.numberOfInputsAndWeights = numberOfInputsAndWeights;
        this.weights = initArrayListToRandom(numberOfInputsAndWeights);
        this.inputs = initArrayListToZero(numberOfInputsAndWeights);
        this.dESlashDyi = initArrayListToZero(numberOfInputsAndWeights);
    }

    public Double feedForward(List<Double> inputs) {
        this.inputs = inputs;
        double sum = sum();
        //LoggerFactory.getLogger(Neuron.class).info("SUM ith neuron " + indexInLayer + ", SUM " + sum);
        this.output = activate.activate(sum);
        //LoggerFactory.getLogger(Neuron.class).info("ACTIVATE ith neuron " + indexInLayer + ", ACTIVATE " + output);
        return this.output;
    }

    public List<Double> backProp(double dESlashDyj) {
        ///final double dESlashDyj = - (predictValue - output);
        this.dESlashDyi = initArrayListToZero(numberOfInputsAndWeights);
        List<Double> weightsModification = new ArrayList<>();
        for (int i = 0; i < numberOfInputsAndWeights; i++) {
            double derivate = activate.derivate(output);
            double baseDown = dESlashDyj * derivate;
            double wBaseMod = inputs.get(i) * baseDown;
            weightsModification.add(HyperParameters.LEARNING_RATE * wBaseMod);
            //LoggerFactory.getLogger(Neuron.class).info("weights modi " + weightsModification.get(i));
            weights.set(i, weights.get(i) - weightsModification.get(i));
            dESlashDyi.set(i, dESlashDyi.get(i) + baseDown * weights.get(i));
        }
        return dESlashDyi;
    }


    private double sum() {
        double zet = 0;
        for (int i = 0; i < inputs.size(); i++) {
            zet += inputs.get(i) * weights.get(i);
        }
        return zet;
    }

    public List<Double> getInputs() {
        return inputs;
    }

    public List<Double> getWeights() {
        return weights;
    }

    public double getOutput() {
        return output;
    }

    public List<Double> getdESlashDyi() {
        return dESlashDyi;
    }

    public void recToString() {
        LoggerFactory.getLogger(Neuron.class).info("neuron " + indexInLayer);
        for (int i = 0; i < inputs.size(); i++) {
            LoggerFactory.getLogger(Neuron.class).info("weight " + weights.get(i));
        }
    }
}
