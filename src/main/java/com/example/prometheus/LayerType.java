package com.example.prometheus;

public enum LayerType {
    OUTPUT,
    MIDDLE,
    INPUT;
}
