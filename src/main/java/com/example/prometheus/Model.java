package com.example.prometheus;

import com.example.prometheus.layers.DenseLayer;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Model {

    private List<DenseLayer> layers = new ArrayList<>();

    public boolean addLayer(DenseLayer denseLayer) {
        return layers.add(denseLayer);
    }

    public void train(List<Double> inputs, List<Double> label) {
        List<Double> actualInputs = predict(inputs);
        //LoggerFactory.getLogger(Model.class).info("Ouputs of net " + actualInputs);
        //backprop
        List<Double> actualError = new ArrayList<>();
        for (int i=0; i < label.size(); i++) {
            actualError.add(-(label.get(i) - actualInputs.get(i)));
        }
        for (int i = layers.size() - 1; i >= 0; i--) {
            actualError = layers.get(i).backProp(actualError);
        }
    }

    public void trainAll(List<List<Double>> inputsAll, List<List<Double>> labelAll) {
        for (int i = 0; i < inputsAll.size(); i++) {
            LoggerFactory.getLogger(Model.class).info("input " + inputsAll.get(i));
            LoggerFactory.getLogger(Model.class).info("labels " + labelAll.get(i));
            train(inputsAll.get(i), labelAll.get(i));
            showModel();
        }
    }

    public List<Double> predict(List<Double> inputs) {
        List<Double> actualInputs = inputs;
        for (int i = 0; i < layers.size(); i++) {
            actualInputs = layers.get(i).feedForward(actualInputs);
        }
        return actualInputs;
    }

    public void showModel() {
        for (int i = 0; i < layers.size(); i++) {
            layers.get(i).recToString();
        }
    }
}
