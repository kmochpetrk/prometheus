package com.example.prometheus;

import com.example.prometheus.layers.DenseLayer;

public class ModelBuilder {

    private Model model = new Model();

    public ModelBuilder addLayer(DenseLayer layer) {
        model.addLayer(layer);
        return this;
    }

    public Model build() {
        return model;
    }

}
