package com.example.prometheus.activations;

public class Sigmoid implements Activate {

    private static final Sigmoid INSTANCE = new Sigmoid();

    @Override
    public double activate(double zet) {
        return 1/(1 + Math.exp(-zet));
    }

    @Override
    public double derivate(double y) {
        return y * (1 - y);
    }

    public static Sigmoid getINSTANCE() {
        return INSTANCE;
    }
}
