package com.example.prometheus.activations;

import java.util.List;

public interface Activate {

    double activate(double zet);

    double derivate(double y);

}
