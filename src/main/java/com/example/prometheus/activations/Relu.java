package com.example.prometheus.activations;

/**
 * Relu activation
 */
public class Relu implements Activate{

    private static final Relu INSTANCE = new Relu();


    @Override
    public double activate(double zet) {
        return zet > 0 ? zet : 0;
    }

    @Override
    public double derivate(double y) {
        return y > 0 ? 1 : 0;
    }

    public static Relu getINSTANCE() {
        return INSTANCE;
    }
}
